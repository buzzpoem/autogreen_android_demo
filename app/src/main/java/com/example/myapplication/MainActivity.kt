package com.example.myapplication

import android.arch.core.util.Function
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.autogreen.agsdk.EVChargingScreen
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val evCharge = EVChargingScreen(this, "123456789010", "ADc1Ft0iySZbWl4kB8hB5wucSXPVf1pZ", "jakgbuierugherwuignewruighiwt87384hgwiuogy")

        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener { view: View? ->
            evCharge.open()
        }

        evCharge.onOpened (
            fun(it: Any?): Void? {
                Log.d("event", "opened")
                return null
            }
        )

        evCharge.onClosed (
            fun(it: Any?): Void? {
                Log.d("event", "closed")
                return null
            }
        )

        evCharge.onChargingDurationSelected { transactionId ->
            Log.d("event", "Transaction ID: $transactionId")

            true
        }

    }
}
